#ifndef __TEST_PERF_H__
#define __TEST_PERF_H__

#include <chrono>
#include "test_tool/test_systool.h"

class Perf {
  public:
    // Constructor and destructor
    Perf();
    ~Perf();

    // Start and end the recording
    void start();
    void end();

    double elapsed_time();
    long int peak_mem();

  private:
    // Start and end clock time
    std::chrono::system_clock::time_point _clock_start;
    std::chrono::system_clock::time_point _clock_end;

    // Initial and peak resident set size
    long int _rss_init;
    long int _rss_peak;
};

#endif
