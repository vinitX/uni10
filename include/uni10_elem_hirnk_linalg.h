#ifndef __UNI10_ELEM_HIRNK_LINALG_H__
#define __UNI10_ELEM_HIRNK_LINALG_H__

#if defined(UNI_LAPACK) && defined(UNI_CPU)
#include "uni10_lapack_cpu/uni10_elem_hirnk_linalg.h"
#endif

// Developing
//
//#if defined(UNI_CUSOLVER) && defined(UNI_GPU)
//#include "uni10_cusolver_gpu/uni10_elem_linalg_cusolver_gpu.h"
//#endif

#endif
